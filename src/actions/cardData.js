export const SET_CARD_DATA = 'SET_CARD_DATA';

export function setCardData(key, value) {
  return {type: SET_CARD_DATA, key, value}
}