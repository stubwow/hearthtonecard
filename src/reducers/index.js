import { combineReducers } from 'redux'
import cardData from './cardData'

const app = combineReducers({
  cardData
});

export default app