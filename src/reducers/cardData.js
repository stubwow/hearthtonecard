import {SET_CARD_DATA} from '../actions/cardData'

const DEFAULT_STATE = {
  name: "Void Terror",
  class: 'neutral',
  race: "Bear",
  rarity: 'common'
};

const cardData = (state = DEFAULT_STATE, action = {}) => {
  switch (action.type) {
    case SET_CARD_DATA:
      return {...state, [action.key]: action.value};
    default:
      return state
  }
};

export default cardData