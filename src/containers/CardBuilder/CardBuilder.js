import React, { Component } from 'react'
import { connect } from 'react-redux'
import convector from 'save-svg-as-png';
import withStyles from '../../../node_modules/isomorphic-style-loader/lib/withStyles';
import s from './CardBuilder.scss';
import {setCardData} from '../../actions/cardData'
import CardView from '../../components/CardView'
import CardEdit from '../../components/CardEdit'
import Tabs, { Pane } from '../../components/Tabs'
import {sendEvent} from '../../utils/Analytics'

class CardBuilder extends Component {

  constructor(props) {
    super(props);
    this.changeData = false;
  }

  componentDidMount() {
    sendEvent('User enter', 'constructor', 'enter', 'User enter');
  }

  convert = () => {
    sendEvent('Save card', 'constructor', 'click', 'Save button');
    convector.saveSvgAsPng(this.refs.cardView.getElementsByTagName('svg')[0], "card.png");
  };

  saveCard = async () => {

    var svg = this.refs.cardView.getElementsByTagName('svg')[0];

    var serializer = new XMLSerializer();
    var source = serializer.serializeToString(svg);

    const resp = await fetch('/api/savesvg', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        svg: source
      })
    });
    const { image } = await resp.json();
    console.log(image);
  };

  changeCardData = (key, value) => {
    this.props.dispatch(setCardData(key, value));
    if(!this.changeData) {
      sendEvent('Change data', 'constructor', 'change', 'Change data');
      this.changeData = true;
    }
  };

  render() {
    return (
      <div className={s.root}>
        <div className={s.left}>
          <Tabs selected={0} titleStyle={s.tabs}>
            <Pane className={s.pane} label="Minion">
              <CardEdit
                cardData={this.props.cardData}
                changeCardData={this.changeCardData}
                convert={() => this.saveCard()}
                />
            </Pane>
            <Pane className={s.pane} label="Spell">
              <div className={s.soon}>SOON AVAILABLE</div>
            </Pane>
            <Pane className={s.pane} label="Weapon">
              <div className={s.soon}>SOON AVAILABLE</div>
            </Pane>
            <Pane className={s.pane} label="Power">
              <div className={s.soon}>SOON AVAILABLE</div>
            </Pane>
          </Tabs>
        </div>
        <div className={s.right} ref="cardView">
          <CardView cardData={this.props.cardData}/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cardData: state.cardData
  }
};

export default connect(mapStateToProps)(withStyles(s)(CardBuilder))
