/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Home.scss';
import CardBuilder from '../../containers/CardBuilder/CardBuilder'

const title = 'Hearthstone card maker';
const description = "This is a web app to create hearthstone fun cards. You may see the result of construction in real-time. The style of card is like of blizzard's style";

function Home(props, context) {
  context.setTitle(title);
  context.setDescription(description);
  return (
    <div className={s.root}>
      <CardBuilder/>
    </div>
  );
}

Home.contextTypes = {
  setTitle: PropTypes.func.isRequired,
  setDescription: PropTypes.func.isRequired
};

export default withStyles(s)(Home);
