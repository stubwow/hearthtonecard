import React, { PropTypes } from 'react';
import Link from '../../components/Link';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './About.scss';

const title = 'Contact Us';
const description = "This is a web app to create hearthstone fun cards. You may see the result of construction in real-time. The style of card is like of blizzard's style";

function About(props, context) {
  context.setTitle(title);
  context.setDescription(description);
  return (
    <div className={s.root}>
        <Link className={s.about} to="/">
          Hs-cards
        </Link>
        <span> - веб приложение для создания фан карт для игры Hearthstone. Наша цель - создать удобный и
        функциональный редактор карт, который позволит каждому без труда составить собственную карту</span>
    </div>
  );
}

About.contextTypes = {
  setTitle: PropTypes.func.isRequired,
  setDescription: PropTypes.func.isRequired
};

export default withStyles(s)(About);