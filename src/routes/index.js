import React from 'react';
import { Provider } from 'react-redux'
import App from '../components/App';
import counterApp from '../reducers'

// Child routes
import home from './home';
import about from './about';

export default {

  path: '/',

  children: [
    home,
    about
  ],

  async action({ next, render, context, store }) {
    const component = await next();
    if (component === undefined) return component;

    return render(
      <Provider store={store}>
        <App context={context}>{component}</App>
      </Provider>

    );
  }

};
