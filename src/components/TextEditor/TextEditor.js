import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import withStyles from '../../../node_modules/isomorphic-style-loader/lib/withStyles';
import s from './TextEditor.scss';
import {Editor, EditorState, RichUtils, Modifier, ContentState} from 'draft-js';

const styleMap = {};
export const sizes = [10, 12, 14, 16, 18, 20, 22, 24];
sizes.map(size =>
    styleMap[size] = {'fontSize': size + 'px'}
);
const defaultFontSize = 14;

class TextEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: EditorState.createEmpty()//this.props.content ? EditorState.createWithContent(this.props.content) : EditorState.createEmpty()
    };
    this.focus = () => this.refs.editor.focus();
  }

  onChange = (editorState) => {
    this.setState({editorState});
    setTimeout(() => {
      this.props.onChange('description', this.state.editorState.getCurrentContent());
      //this.props.onChange('descriptionContent', this.state.editorState.getCurrentContent());
    });
  };

  toggleInlineStyle = (e, inlineStyle) => {
    e.preventDefault();
    this.onChange(
      RichUtils.toggleInlineStyle(
        this.state.editorState,
        inlineStyle
      )
    );
  };

  changeFontSize = (e, toggleSize) => {
    e.preventDefault();
    const {editorState} = this.state;
    const selection = editorState.getSelection();

    const nextContentState = Object.keys(styleMap)
      .reduce((contentState, size) => {
        return Modifier.removeInlineStyle(contentState, selection, size)
      }, editorState.getCurrentContent());
    let nextEditorState = EditorState.push(
      editorState,
      nextContentState,
      'change-inline-style'
    );

    const currentStyle = editorState.getCurrentInlineStyle();
    nextEditorState = currentStyle.reduce((state, size) => {
      return RichUtils.toggleInlineStyle(state, size);
    }, nextEditorState);

    if (!currentStyle.has(toggleSize)) {
      nextEditorState = RichUtils.toggleInlineStyle(
        nextEditorState,
        toggleSize
      );
    }

    this.onChange(nextEditorState);
  };

  render() {
    var currentStyle = this.state.editorState.getCurrentInlineStyle();
    const sizeActiveCondition = (size) => {
      if (size == defaultFontSize) {
        //TODO bug with defaultFontSize
        return (currentStyle.size == 0
          || (currentStyle.has('BOLD') && currentStyle.size == 1)
          || (currentStyle.has('ITALIC') && currentStyle.has('BOLD') && currentStyle.size == 2)
        );
      } else {
        return currentStyle.has(size);
      }
    };
    return (
      <div className={s.root}>
        <div className={s.controls}>
          {['Bold', 'Italic'].map(type =>
              <div className={classnames(s.button, currentStyle.has(type.toUpperCase()) ? s.active : '')}
                   onMouseDown={(e) => this.toggleInlineStyle(e,type.toUpperCase())}
                   key={type}>
                {type}
              </div>
          )}
          <div className={s.separator}></div>
          {sizes.map(size =>
              <div className={classnames(s.button, sizeActiveCondition(size) ? s.active : '')}
                   onMouseDown={(e) => this.changeFontSize(e,size)}
                   key={size}>
                {size + 'px'}
              </div>
          )}
        </div>
        <div className={s.editor} onClick={this.focus}>
          <Editor ref="editor"
                  customStyleMap={styleMap}
                  editorState={this.state.editorState}
                  onChange={(e) => this.onChange(e)}
            />
        </div>
      </div>
    )
  }
}

TextEditor.propTypes = {
  content: PropTypes.object,
  onChange: PropTypes.func.isRequired
};


export default withStyles(s)(TextEditor)