import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import withStyles from '../../../node_modules/isomorphic-style-loader/lib/withStyles';
import s from './Tabs.scss';

class Tabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: this.props.selected
    };
  }

  handleClick = (index, event) => {
    event.preventDefault();
    this.setState({
      selected: index
    });
  };

  renderTitles = () => {
    function labels(child, index) {
      let activeClass = (this.state.selected === index ? s.active : '');
      return (
        <li key={index}>
          <a href="#"
             className={activeClass}
             onClick={this.handleClick.bind(this, index)}>
            {child.props.label}
          </a>
        </li>
      );
    }

    return (
      <ul className={classNames(this.props.titleStyle,s.tabsLabels)}>
        {this.props.children.map(labels.bind(this))}
      </ul>
    )
  };

  renderContent = () => {
    return (
      <div className={s.tabsContent}>
        {this.props.children[this.state.selected]}
      </div>
    );
  };

  render() {
    return (
      <div className={s.tabs}>
        {this.renderTitles()}
        {this.renderContent()}
      </div>
    );
  }
}

Tabs.defaultProps = {
  selected: 0
};

Tabs.propTypes = {
  selected: PropTypes.number,
  titleStyle: PropTypes.string
};

export default withStyles(s)(Tabs);

export class Pane extends Component {

  render() {
    return (
      <div {...this.props}>
        {this.props.children}
      </div>
    );
  }
}

Pane.propTypes = {
  label: React.PropTypes.string.isRequired,
  children: React.PropTypes.element.isRequired
};


