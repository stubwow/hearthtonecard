import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CardEdit.scss';
import TextEditor from '../TextEditor/TextEditor';
import { classes, rarity } from '../../config';

class CardEdit extends Component {

  saveCard = (e) => {
    e.preventDefault();
    this.props.convert();
  };

  render() {
    const { cardData, changeCardData } = this.props;
    return (
      <form className={s.form} onChange={e => changeCardData(e.target.name, e.target.value)}>
        <div className={s.formGroup}>
          <div className={s.label}>Card name and tribe (optionaly)</div>
          <input className={classNames(s.name, s.input)} name='name' type='text' maxLength="30" defaultValue={cardData.name}
                 placeholder='Card name'/>
          <input className={classNames(s.race, s.input)} name='race' type='text' defaultValue={cardData.race}
                 placeholder='Tribe'/>
        </div>
        <div className={s.formGroup}>
          <div className={s.label}>Choose a class</div>
          <div className={s.classBox}>
            { classes.map((cls, i) =>
                <div className={s.class}
                     style={{backgroundImage: 'url(images/classes/' + cls + '.png)'}}
                     key={i}
                     onClick={() => changeCardData('class', cls)}>
                </div>
            )}
          </div>
        </div>

        <div className={classNames(s.formGroup,s.cahBlock)}>
          <div className={s.label}>Cost, attack and health</div>
          <input className={classNames(s.input, s.cost)} name='cost' type='text' defaultValue={cardData.cost}
                 placeholder='cost' maxLength="2"/>
          <input className={classNames(s.input, s.attack)} name='attack' type='text' defaultValue={cardData.attack}
                 placeholder='attack' maxLength="2"/>
          <input className={classNames(s.input, s.health)} name='health' type='text' defaultValue={cardData.health}
                 placeholder='health' maxLength="2"/>
        </div>
        <div className={classNames(s.formGroup, s.rarityContent)}>
          <div className={s.label}>Choose a rarity</div>
          <div className={s.rarityBox}>
          { rarity.map((rarity, i) =>
              <div className={s.rarity}
                   style={{backgroundImage: 'url(images/rarity/gems/' + rarity + '.png)'}}
                   key={i}
                   onClick={() => changeCardData('rarity', rarity)}>
              </div>
          )}</div>
        </div>
        <div className={s.formGroup}>
          <div className={s.label}>Card text</div>
          <div className={s.description}>
            <TextEditor
              content={cardData.descriptionContent}
              onChange={this.props.changeCardData}/>
          </div>
        </div>
        <div className={s.save}>
          <button className={s.button} onClick={this.saveCard}>save</button>
        </div>
      </form>

    )
  }

}

CardEdit.propTypes = {
  cardData: PropTypes.object.isRequired,
  changeCardData: PropTypes.func.isRequired,
  convert: PropTypes.func.isRequired
};

export default withStyles(s)(CardEdit);
