import React, { PropTypes, Component, classnames } from 'react';
import ReactDOM from 'react-dom';
import withStyles from '../../../../node_modules/isomorphic-style-loader/lib/withStyles';
import s from './Zoom.scss';

export default class Zoom extends Component {
  minScroll = 0;
  maxScroll = 100;

  constructor(props) {
    super(props);
    this.state = {
      scrollPosition: this.maxScroll
    };
    this.drag = false;
    this.shiftY = 0;
  }

  zoomIn = () => {
    let scroll = this.state.scrollPosition - this.maxScroll*this.props.scaleStep;
    if(scroll < this.minScroll) {
      scroll = this.minScroll;
    }
    this.scrolling(scroll);
  };

  zoomOut = () => {
    let scroll = this.state.scrollPosition + this.maxScroll * this.props.scaleStep;
    if(scroll > this.maxScroll) {
      scroll = this.maxScroll;
    }
    this.scrolling(scroll);
  };

  mouseDown = (e) => {
    e.preventDefault();
    let pageY = e.type == 'touchstart' ? e.changedTouches[0].pageY : e.pageY;
    this.drag = true;
    this.shiftY = pageY - this.state.scrollPosition;
  };

  mouseMove = (e) => {
    e.preventDefault();
    if (this.drag) {
      let pageY = e.type == 'touchmove' ? e.changedTouches[0].pageY : e.pageY;
      if (!(pageY - this.shiftY < this.minScroll || pageY - this.shiftY > this.maxScroll)) {
        this.scrolling(pageY - this.shiftY);
      }
    }
  };

  scrolling = (scroll) => {
    let dy = 1 - scroll / (this.maxScroll - this.minScroll);
    this.props.scaleChange(this.props.minScale + (this.props.maxScale - this.props.minScale) * dy);
    this.setState({
      scrollPosition: scroll
    });
  };

  mouseUp = (e) => {
    e.preventDefault();
    this.drag = false;
  };

  render() {
    return (
      <div className={s.root}
           onMouseMove={this.mouseMove}
           onTouchMove={this.mouseMove}
           onMouseUp={this.mouseUp}
           onTouchEnd={this.mouseUp}
           onMouseLeave={this.mouseUp}>
        <div className={s.button} onClick={this.zoomIn}>+</div>
        <div className={s.scroll}>
          <div className={s.caret} style={{top : this.state.scrollPosition + 'px'}}
               onTouchStart={this.mouseDown}
               onMouseDown={this.mouseDown}>
          </div>
        </div>
        <div className={s.button} onClick={this.zoomOut}>-</div>
      </div>
    )
  }
}

Zoom.propTypes = {
  scaleStep: PropTypes.number.isRequired,
  scale: PropTypes.number.isRequired,
  minScale: PropTypes.number.isRequired,
  maxScale: PropTypes.number.isRequired,
  scaleChange: PropTypes.func.isRequired
};

Zoom.defaultProps = {
  scaleStep: 0.2
};

export default withStyles(s)(Zoom);
