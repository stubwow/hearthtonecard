import React, { PropTypes, Component, classnames } from 'react';
import ReactDOM from 'react-dom';
import Dropzone from 'react-dropzone';
import { getEntityRanges } from 'draft-js-utils';
import CardAvatar from './CardAvatar';
import _ from "lodash";
import {sizes} from '../TextEditor';
import withStyles from '../../../node_modules/isomorphic-style-loader/lib/withStyles';
import s from './CardView.scss';

const maxNameWidth = 265;
const defaultNameSize = 26;

export default class CardView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      file: null,
      scale: 1,
      nameSize: defaultNameSize
    };
    this.calcNameSize = false;
  }

  onDrop = (file) => {
    var reader  = new FileReader();
    reader.onloadend = (() => {
      this.setState({
        file: reader.result
      });
    }).bind(this);
    reader.readAsDataURL(file[0]);
  };

  renderLine = (stylePieces) => {
    let maxSize = 14;
    return {
      lines: stylePieces.map(([text, style], id) => {
        let textStyle = {fontSize: '18px'};
        if (style.has('BOLD')) {
          textStyle.fontWeight = 'bold';
        }
        if (style.has('ITALIC')) {
          textStyle.fontStyle = 'italic';
        }
        _.map(sizes, (size)=> {
          if (style.has(size)) {
            textStyle.fontSize = size + 4 + 'px';
            maxSize = maxSize < size ? size : maxSize;
          }
        });
        return <tspan key={id} style={textStyle}>{text}</tspan>;
      }),
      size: maxSize
    }
  };

  renderBlocks = (blocks) => {
    let dy = 0;
    return {
      blocks: _.map(blocks, (block, i) => {
        let entityPieces = getEntityRanges(block.getText(), block.getCharacterList());
        return entityPieces.map(([entityKey, stylePieces]) => {
          let renderedLine = this.renderLine(stylePieces);
          i == 0 ? dy = 0 : dy += renderedLine.size * 1.2;
          return (
            <text x='190' y='366' dy={dy + 'px'} style={{textAnchor: 'middle'}}>
              {
                renderedLine.lines
              }
            </text>)
        });
      }),
      height: dy
    }
  };

  renderDescription = (content) => {
    let renderedBlocks = this.renderBlocks(content.getBlockMap().toArray());
    return (
      <g transform={'translate(0,'+ -renderedBlocks.height/2+')'}>
        {
          renderedBlocks.blocks
        }
      </g>
    )
  };

  componentDidUpdate() {
    if (!this.calcNameSize) {
      this.setState({
        nameSize: defaultNameSize
      });
      this.calcNameSize = true;
    } else {
      let width = this.refs.name.getBBox().width;
      if (width > maxNameWidth) {
        this.setState({
          nameSize: this.state.nameSize - 1
        });
      } else {
        this.calcNameSize = false;
      }
    }
  }

  render() {
    const { cardData } = this.props;
    const name = {
      fontFamily: 'Belwe Bd BT',
      fontSize: this.state.nameSize + 'px',
      textAnchor: 'middle',
      letterSpacing: '1px'
    };
    const text = {
      fontFamily: 'Belwe Bd BT',
      fontSize: '24px',
      textAnchor: 'middle',
      letterSpacing: '1px'
    };
    const cost = {
      fontFamily: 'Belwe Bd BT',
      fontSize: '80px',
      textAnchor: 'middle'
    };
    const num = {
      fontFamily: 'Belwe Bd BT',
      fontSize: '76px',
      textAnchor: 'middle'
    };

    return (
      <div className={s.root}>
        <Dropzone className={s.drop}
                  activeClassName={s.active}
                  rejectClassName={s.reject}
                  multiple={false}
                  accept={'image/*'}
                  onDropAccepted={this.onDrop}>
          <div >Drop some image here, or click to select image to upload.</div>
        </Dropzone>
        <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
             className={s.svg} viewBox="0 0 354 487"
             style={{overflow: 'visible', pointerEvents: this.state.file ? 'all' : 'none'}} ref='svg'
             version="1.1"
             width='100%' height='100%'>
          <defs>

            <path id="MyPath"
                  d="m 50.336299,276.51481 c 0,0 3.8284,6.45932 52.003431,0.99221 28.84576,-5.27354 57.87325,-12.4128 107.80271,-18.66988 36.69487,-4.59854 70.68333,-1.86349 70.95307,0.16154 25.31819,5.0831 43.46829,20.47526 43.46829,12.47526"></path>

            <filter id='glow'>
              <feMorphology in='SourceGraphic' operator='dilate' radius='2' result='expand'/>
              <feMerge result='shadow'>
                <feMergeNode in='expand'/>
              </feMerge>
              <feFlood floodColor='#000000' floodOpacity="0.8"/>
              <feComposite in2='shadow' operator='in' result='shadow'/>
              <feMerge>
                <feMergeNode in='shadow'/>
                <feMergeNode in='SourceGraphic'/>
              </feMerge>
            </filter>

          </defs>
          <CardAvatar x={80} y={10} width={250} height={260}
                      image={this.state.file}
                      scale={this.state.scale}
                      closeClick={() => this.setState({file: null})}/>
          <image xlinkHref={'/images/cardTemplate/'+cardData.class+'.png'} x='0' y='0' width='354' height='487'
                 style={{pointerEvents:'none'}}/>
          {cardData.rarity != 'basic' &&
          <image xlinkHref={'/images/rarity/'+cardData.rarity+'.png'} x='160' y='272' width='69' height='42'/>
          }
          {cardData.rarity == 'legend' &&
          <image xlinkHref={'/images/rarity/dragon.png'} x='84' y='-23' width='276' height='261'/>
          }
          <text fill='#ffffff' ref="name" filter="url(#glow)" style={name}>
            <textPath startOffset="50%" xlinkHref="#MyPath">
              {cardData.name}
            </textPath>
          </text>
          {cardData.race && <image xlinkHref={'/images/race.png'} x='98' y='414' width='188' height='42'/>}
          <text x='190' y='448' fill='#ffffff' filter="url(#glow)" style={text}>{cardData.race}</text>
          <text x='58' y='74' fill='#ffffff' filter="url(#glow)" style={cost}>{cardData.cost}</text>
          <text x='62' y='460' fill='#ffffff' filter="url(#glow)" style={num}>{cardData.attack}</text>
          <text x='316' y='460' fill='#ffffff' filter="url(#glow)" style={num}>{cardData.health}</text>
          {cardData.description && this.renderDescription(cardData.description)}
        </svg>

      </div>
    )
  }
}

CardView.propTypes = {
  cardData: PropTypes.object.isRequired
};

export default withStyles(s)(CardView);
