import React, { PropTypes, Component, classnames } from 'react';
import Zoom from '../Zoom'
import ReactDOM from 'react-dom';
import withStyles from '../../../../node_modules/isomorphic-style-loader/lib/withStyles';
import s from './CardAvatar.scss';

export default class CardAvatar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      x: 0,
      y: 0,
      width: 0,
      height: 0,
      scale: 1,
      minScale: 0.1,
      maxScale: 1
    };
    this.drag = false;
    this.shiftX = 0;
    this.shiftY = 0;
  }

  componentDidUpdate(prevProps) {
    if (this.props.image && prevProps.image != this.props.image) {
      let img = new Image();
      img.src = this.props.image;
      img.onload = () => {
        this.width = img.width;
        this.height = img.height;
        this.placeImage();
      }
    }
  }

  placeImage = () => {
    let scale, x, y;
    if (this.width > this.height) {
      scale = this.props.height / this.height;
      x = -this.props.width / 2;
      y = 0;
    } else {
      scale = this.props.width / this.width;
      x = 0;
      y = -this.props.height / 2;
    }
    this.setState({
      scale: scale,
      minScale: scale,
      x: x,
      y: y,
      width: this.width * scale,
      height: this.height * scale
    });
  };

  changeScale = (scale) => {
    if (!(this.props.height > Math.round(this.height * scale) || this.props.width > Math.round(this.width * scale))) {
      let x = this.state.x - (this.width * (scale - this.state.scale)) / 2,
        y = this.state.y - (this.height * (scale - this.state.scale)) / 2;
      if (x > 0) {
        x = 0
      }
      if (x < -this.width * scale + this.props.width) {
        x = -this.width * scale + this.props.width
      }
      if (y > 0) {
        y = 0
      }
      if (y < -this.height * scale + this.props.height) {
        y = -this.height * scale + this.props.height
      }
      this.setState({
        scale: scale,
        width: this.width * scale,
        height: this.height * scale,
        x: x,
        y: y
      });
    }
  };

  mouseDown = (e) => {
    e.preventDefault();
    let pageX = e.type == 'touchstart' ? e.changedTouches[0].pageX : e.pageX,
      pageY = e.type == 'touchstart' ? e.changedTouches[0].pageY : e.pageY;
    this.drag = true;
    this.shiftX = pageX - this.state.x;
    this.shiftY = pageY - this.state.y;
  };

  mouseMove = (e) => {
    e.preventDefault();
    if (this.drag) {
      let pageX = e.type == 'touchmove' ? e.changedTouches[0].pageX : e.pageX,
        pageY = e.type == 'touchmove' ? e.changedTouches[0].pageY : e.pageY,
        x = this.state.x,
        y = this.state.y;
      if (!(pageX - this.shiftX > 0 || pageX - this.shiftX < -this.state.width + this.props.width)) {
        x = pageX - this.shiftX
      }
      if (!(pageY - this.shiftY > 0 || pageY - this.shiftY < -this.state.height + this.props.height)) {
        y = pageY - this.shiftY
      }
      this.setState({
        x: x,
        y: y
      });
    }
  };

  mouseUp = (e) => {
    e.preventDefault();
    this.drag = false;
  };

  render() {
    return (
      <g>
        <svg  {...this.props} >
          <defs>
            <clipPath id="myClip">
              <path d="M50 30 L80 0 L130 0 L180 30 L210 150 L210 240 L0 240 L0 100 Z"/>
            </clipPath>
          </defs>

          <image xlinkHref={this.props.image}
                 x={this.state.x} y={this.state.y}
                 width={this.state.width} height={this.state.height}
                 clipPath="url(#myClip)"
                 style={{cursor: 'pointer'}}
                 onMouseDown={this.mouseDown}
                 onTouchStart={this.mouseDown}
                 onMouseMove={this.mouseMove}
                 onTouchMove={this.mouseMove}
                 onMouseUp={this.mouseUp}
                 onTouchEnd={this.mouseUp}
                 onMouseLeave={this.mouseUp}>
          </image>

        </svg>
        {this.props.image &&
        <foreignObject x="350" y="30" width="30" height="200">
          <div style={{position: 'fixed'}}>
            <div className={s.close} onClick={() => this.props.closeClick()}>+</div>
            <Zoom scale={this.state.scale} minScale={this.state.minScale} maxScale={this.state.maxScale}
                  scaleChange={this.changeScale}/>
          </div>
        </foreignObject>
        }
      </g>
    )
  }
}

CardAvatar.propTypes = {
  image: PropTypes.string
};

export default withStyles(s)(CardAvatar);
