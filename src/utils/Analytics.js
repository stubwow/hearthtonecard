export function sendEvent (mixpanelEvent, gaEventCategory, gaEventAction, gaEventLabel) {
  if(window.mixpanel) {
    window.mixpanel.track(mixpanelEvent);
  }
  if(ga) {
    ga('send', 'event', gaEventCategory, gaEventAction, gaEventLabel);
  }
};